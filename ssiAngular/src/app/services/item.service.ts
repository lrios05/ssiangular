import {Injectable} from '@angular/core';
import {Item} from '../shared/item';
// import {ITEMS} from '../shared/items';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/delay';
import 'rxjs/add/observable/of';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {baseURL} from '../shared/baseurl';

@Injectable({
  providedIn: 'root'
})
export class ItemService {

  constructor(private http: HttpClient) {
  }

  // getItems(): Observable<Item[]> {
  //   return Observable.of(ITEMS).delay(2000);
  // }
  //
  // getItem(id: number): Observable<Item> {
  //   return Observable.of(ITEMS.filter((item) => (item.id === id))[0]).delay(2000);
  // }
  //
  // getFeaturedItem(): Observable<Item> {
  //   return Observable.of(ITEMS.filter((item) => item.featured)[0]).delay(2000);
  // }
  //
  // getItemIds(): Observable<number[]> {
  //   return Observable.of(ITEMS.map(item => item.id));
  // }

  getItems(): Observable<Item[]> {
    return <Observable<Item[]>> this.http.get(baseURL + 'items');
  }

  getItem(id: number): Observable<Item> {
    return <Observable<Item>>this.http.get(baseURL + 'items/' + id);
  }

  getFeaturedItem(): Observable<Item> {
    return <Observable<Item>> this.http.get(baseURL + 'items?featured=true');
  }

  getItemIds(): Observable<number[]> {
    return <Observable<number[]>> this.http.get(baseURL + 'items')
      .pipe(map(items => (<Item[]>items).map(item => item.id)));
  }
}
