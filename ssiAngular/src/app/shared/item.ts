import {Comment} from './comment';

export class Item { // si no hay funciones es preferible usar interface
  id: number;
  featured: boolean;
  name: string;
  image: string;
  category: string;
  label: string;
  price: string;
  description: string;
  comments: Comment[];
}
