import {Component, OnInit} from '@angular/core';
import {Item} from '../shared/item';
import {ItemService} from '../services/item.service';
import {ActivatedRoute, Params} from '@angular/router';
import {Location} from '@angular/common';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-item-detail',
  templateUrl: './item-detail.component.html',
  styleUrls: ['./item-detail.component.scss']
})
export class ItemDetailComponent implements OnInit {
  item: Item;
  itemIds: number[];
  prev: number;
  next: number;

  constructor(private itemService: ItemService, private route: ActivatedRoute, private location: Location) {
  }

  ngOnInit() {
    // const id = +this.route.snapshot.params['id'];
    // this.item = this.itemService.getItem(id);
    // this.itemService.getItem(id).subscribe(item => this.item = item);
    this.itemService.getItemIds().subscribe(items => {
      this.itemIds = items;
      this.route.params.switchMap((params: Params) => this.itemService.getItem(+params['id'])).subscribe(item => {
        this.item = item;
        this.setPrevNext(item.id);
      });
    });
  }

  goBack(): void {
    this.location.back();
  }

  private setPrevNext(itemId: number) {
    const index = this.itemIds.indexOf(itemId);
    this.prev = this.itemIds[(this.itemIds.length + index - 1) % this.itemIds.length];
    this.next = this.itemIds[(this.itemIds.length + index + 1) % this.itemIds.length];
  }
}
